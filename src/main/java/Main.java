import org.bouncycastle.cms.CMSException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class Main {
    public static void main(String[] args) {
        String secretMessage = "My password is 123456Seven";
        String path_file_public = "E:\\! University\\Магистратура\\1 курс\\1 сем\\САБД\\lab2\\src\\main\\resources\\public.cer";
        String path_file_private = "E:\\! University\\Магистратура\\1 курс\\1 сем\\САБД\\lab2\\src\\main\\resources\\private.p12";

        Security.addProvider(new BouncyCastleProvider());
        CertificateFactory certFactory = null;
        try {
            certFactory = CertificateFactory
                    .getInstance("X.509", "BC");
        } catch (CertificateException | NoSuchProviderException e) {
            e.printStackTrace();
        }
        X509Certificate certificate = null;
        try {
            assert certFactory != null;
            certificate = (X509Certificate) certFactory
                    .generateCertificate(new FileInputStream(path_file_public));
        } catch (CertificateException | FileNotFoundException e) {
            e.printStackTrace();
        }
        char[] keystorePassword = "password".toCharArray();
        char[] keyPassword = "password".toCharArray();
        KeyStore keystore = null;
        try {
            keystore = KeyStore.getInstance("PKCS12");
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        try {
            assert keystore != null;
            keystore.load(new FileInputStream(path_file_private), keystorePassword);
        } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
            e.printStackTrace();
        }
        PrivateKey privateKey = null;
        try {
            privateKey = (PrivateKey) keystore.getKey("baeldung",
                    keyPassword);
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
            e.printStackTrace();
        }

        System.out.println("Original Message : " + secretMessage);
        byte[] stringToEncrypt = secretMessage.getBytes();
        byte[] encryptedData = new byte[0];
        try {
            encryptedData = Encryptor.encryptData(stringToEncrypt, certificate);
        } catch (CertificateEncodingException | CMSException | IOException e) {
            e.printStackTrace();
        }
        System.out.println("Encrypted Message : " + new String(encryptedData));
        byte[] rawData = new byte[0];
        try {
            rawData = Encryptor.decryptData(encryptedData, privateKey);
        } catch (CMSException e) {
            e.printStackTrace();
        }
        String decryptedMessage = new String(rawData);
        System.out.println("Decrypted Message : " + decryptedMessage);


        byte[] signedData = new byte[0];
        try {
            signedData = Signature.signData(rawData, certificate, privateKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Boolean check = null;
        try {
            check = Signature.verifySignedData(signedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Verify: " + check);
    }
}
